module TextColorPicker exposing (Model, Msg, initialModel, listenScreenClick, screen_click, update, view)

import Browser.Dom as Dom
import Color exposing (Color)
import Color.Convert exposing (colorToHexWithAlpha, hexToColor)
import ColorPicker
import Html exposing (Html, button, div, input, span, text)
import Html.Attributes as Ha exposing (..)
import Html.Events exposing (onClick, onInput, onMouseEnter, onMouseLeave)
import Task



-- MODEL


type alias Model =
    { pickerState : ColorPicker.State
    , color : Color
    , text : String
    , visiblePicker : Bool
    , enter : Bool
    , clickOpener : Bool
    }


initialModel : Color -> Model
initialModel color =
    { pickerState = ColorPicker.empty
    , color = color
    , text = color |> colorToHexWithAlpha
    , visiblePicker = False
    , enter = False
    , clickOpener = False
    }


listenScreenClick : Model -> Bool
listenScreenClick model =
    model.clickOpener || not model.enter



-- UPDATE


type Msg
    = Picker ColorPicker.Msg
    | TextInput String
    | ClickOpener
    | Enter
    | Leave


update : Msg -> Model -> Model
update msg model =
    case msg of
        Picker pickerMsg ->
            let
                ( m, maybe_colour ) =
                    ColorPicker.update pickerMsg model.color model.pickerState

                colour =
                    maybe_colour |> Maybe.withDefault model.color
            in
            { model
                | pickerState = m
                , color = colour
                , text = colour |> colorToHexWithAlpha
            }

        TextInput text ->
            { model
                | text = text
                , color = text |> hexToColor |> Result.withDefault model.color
            }

        ClickOpener ->
            { model
                | clickOpener = True
            }

        Enter ->
            { model
                | enter = True
            }

        Leave ->
            { model
                | enter = False
            }


screen_click model =
    { model
        | visiblePicker =
            if model.clickOpener then
                True

            else if model.enter then
                model.visiblePicker

            else
                False
        , clickOpener = False
    }



-- VIEW


view : Model -> Html Msg
view model =
    span []
        [ input [ value <| model.text, onInput <| TextInput ] []
        , button [ onClick ClickOpener ] [ text ">" ]
        , floating_picker model
        ]


floating_picker model =
    let
        childs =
            if model.visiblePicker then
                [ ColorPicker.view model.color model.pickerState |> Html.map Picker ]

            else
                []
    in
    span [ style "width" "0px" ]
        [ span [ style "position" "absolute", onMouseEnter Enter, onMouseLeave Leave ] childs
        ]

module SizeSelector exposing (Model, Msg, initialModel, update, view)

import Html exposing (Html, input, option, select, span, text)
import Html.Attributes as Ha exposing (..)
import Html.Events exposing (onInput)
import List



-- MODEL


type alias Model =
    { widthText : String
    , heightText : String
    , width : Int
    , height : Int
    }


initialModel : Model
initialModel =
    { widthText = "1920"
    , heightText = "1080"
    , width = 1920
    , height = 1080
    }



-- UPDATE


type Msg
    = PresetSelect String
    | WidthInput String
    | HeightInput String


update : Msg -> Model -> Model
update msg model =
    case msg of
        PresetSelect select ->
            let
                splitted =
                    if select |> String.isEmpty then
                        []

                    else
                        select |> String.split "x"

                first =
                    splitted |> List.head |> Maybe.withDefault model.widthText

                second =
                    splitted |> List.drop 1 |> List.head |> Maybe.withDefault model.heightText
            in
            { model
                | widthText = first
                , heightText = second
                , width = first |> String.toInt |> Maybe.withDefault model.width
                , height = second |> String.toInt |> Maybe.withDefault model.height
            }

        WidthInput val ->
            { model | widthText = val, width = val |> String.toInt |> Maybe.withDefault model.width }

        HeightInput val ->
            { model | heightText = val, height = val |> String.toInt |> Maybe.withDefault model.height }



-- VIEW


view : Model -> Html Msg
view model =
    span []
        [ input [ value <| model.widthText, onInput <| WidthInput ] []
        , input [ value <| model.heightText, onInput <| HeightInput ]
            []
        , select
            [ onInput <| PresetSelect, value <| toSelectValue model ]
            [ option
                [ value "640x360" ]
                [ text "640×360 (360p)" ]
            , option
                [ value "720x480" ]
                [ text "720×480 (480p)" ]
            , option
                [ value "1280x720" ]
                [ text "1280×720 (720p)" ]
            , option
                [ value "1920x1080", selected True ]
                [ text "1920×1080 (1080p)" ]
            , option
                [ value "2560x1440" ]
                [ text "2560×1440 (1440p)" ]
            , option
                [ value "3840x2160" ]
                [ text "3840×2160 (2160p )" ]
            , option
                [ value "7680x4320" ]
                [ text "7680×4320 (4320p)" ]
            ]
        ]


toSelectValue model =
    (model.width |> String.fromInt) ++ "x" ++ (model.height |> String.fromInt)

module Main exposing (..)

import Array
import Browser
import Browser.Events as Events
import Color exposing (Color)
import Color.Convert exposing (colorToHexWithAlpha)
import File.Download as Download
import Html exposing (..)
import Html.Attributes as Ha exposing (..)
import Html.Events exposing (onClick, onInput)
import Json.Decode as D
import List
import Random
import Random.List
import SizeSelector as Ss exposing (..)
import Svg.String exposing (Svg, circle, g, rect, svg, toHtml, toString)
import Svg.String.Attributes as Sa exposing (..)
import TextColorPicker as Cp exposing (..)


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type alias Model =
    { seed : Int
    , backgroundColorPicker : Cp.Model
    , circleColors : List Cp.Model
    , size : Ss.Model
    , num : Int
    , circleSize : Float
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { seed = 0
      , backgroundColorPicker = Cp.initialModel <| Color.white
      , circleColors = (Cp.initialModel <| Color.black) |> List.singleton
      , size = Ss.initialModel
      , num = 20
      , circleSize = 0.33
      }
    , Cmd.none
    )


listenScreenClick : Model -> Bool
listenScreenClick model =
    (model.backgroundColorPicker |> Cp.listenScreenClick)
        || (model.circleColors |> List.any Cp.listenScreenClick)



-- UPDATE


type Msg
    = GenerateSeed
    | ChangeSeed String
    | ChangeSeedInt Int
    | ChangeBackgroundColor Cp.Msg
    | ChangeCircleColor Int Cp.Msg
    | IncreaseColor
    | DecreaseColor
    | ChangeNum String
    | ChangeCircleSize String
    | ChangeSize Ss.Msg
    | DownloadSvg
    | ScreenClick


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeSeed newContent ->
            ( { model
                | seed =
                    newContent
                        |> String.toInt
                        |> Maybe.withDefault 0
              }
            , Cmd.none
            )

        ChangeSeedInt newContent ->
            ( { model | seed = newContent }, Cmd.none )

        GenerateSeed ->
            ( model
            , Random.generate ChangeSeedInt (Random.int 1 10000000)
            )

        ChangeBackgroundColor pickerTextMsg ->
            ( { model
                | backgroundColorPicker = Cp.update pickerTextMsg model.backgroundColorPicker
              }
            , Cmd.none
            )

        ChangeCircleColor index pickerTextMsg ->
            ( { model
                | circleColors =
                    model.circleColors
                        |> List.indexedMap
                            (\i color ->
                                if i == index then
                                    Cp.update pickerTextMsg color

                                else
                                    color
                            )
              }
            , Cmd.none
            )

        ScreenClick ->
            ( { model
                | backgroundColorPicker =
                    Cp.screen_click model.backgroundColorPicker
                , circleColors =
                    model.circleColors
                        |> List.map
                            (\color ->
                                Cp.screen_click color
                            )
              }
            , Cmd.none
            )

        IncreaseColor ->
            ( { model
                | circleColors =
                    List.append model.circleColors [ Cp.initialModel <| Color.black ]
              }
            , Cmd.none
            )

        DecreaseColor ->
            ( { model
                | circleColors =
                    List.take (List.length model.circleColors - 1) model.circleColors
              }
            , Cmd.none
            )

        ChangeNum newContent ->
            ( { model
                | num =
                    newContent
                        |> String.toInt
                        |> Maybe.withDefault 0
              }
            , Cmd.none
            )

        ChangeCircleSize newContent ->
            ( { model
                | circleSize =
                    newContent
                        |> String.toFloat
                        |> Maybe.withDefault 0
              }
            , Cmd.none
            )

        ChangeSize changeSize ->
            ( { model
                | size = Ss.update changeSize model.size
              }
            , Cmd.none
            )

        DownloadSvg ->
            ( model, model |> svg_area_text |> download )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.backgroundColorPicker |> Cp.listenScreenClick then
        Events.onClick (D.succeed ScreenClick)

    else
        Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ header []
            [ h1 [] [ text "円をランダムに配置した画像ジェネレーター" ]
            ]
        , main_ [ class "" ]
            [ controller model
            , preview model
            ]
        , footer []
            [ ul []
                [ li [] [ text "円をランダムに配置した画像を生成します" ]
                , li [] [ text "IEではうまく動きません" ]
                , li [] [ text "生成してダウンロードした画像の権利は利用者様に帰属します" ]
                , li [] [ text "本サイトを利用したことにより発生した不都合・損害について製作者は一切の責任を負いません" ]
                , li []
                    [ a
                        [ href "https://gitlab.com/matunnkazumi/maru-no-gazou-generator/"
                        , target "_blank"
                        , class "underline"
                        ]
                        [ text "ソースコード" ]
                    ]
                ]
            ]
        ]


controller model =
    section [ class "controller" ]
        [ h2 [ class "section_title" ] [ text "パラメータ" ]
        , dl []
            [ dt [] [ text "シード値" ]
            , dd []
                [ input [ value (String.fromInt model.seed), onInput ChangeSeed ] []
                , button [ onClick GenerateSeed ] [ text "Seed" ]
                ]
            ]
        , dt [] [ text "背景色" ]
        , dd []
            [ Cp.view model.backgroundColorPicker |> Html.map ChangeBackgroundColor
            ]
        , dt []
            [ text "丸の色" ]
        , dd
            []
            [ circle_color_input_list model
            , button [ onClick IncreaseColor ] [ text "+" ]
            , button
                [ onClick DecreaseColor
                , disabled <| (model.circleColors |> List.length) <= 1
                ]
                [ text "-" ]
            ]
        , dt [] [ text "丸の数" ]
        , dd []
            [ input
                [ model.num |> String.fromInt |> value
                , onInput ChangeNum
                , Ha.type_ "range"
                , Ha.max "100"
                , Ha.min "1"
                ]
                []
            , span [] [ model.num |> String.fromInt |> text ]
            ]
        , dt [] [ text "丸の大きさ" ]
        , dd []
            [ input
                [ model.circleSize |> String.fromFloat |> value
                , onInput ChangeCircleSize
                , Ha.type_ "range"
                , Ha.max "1.0"
                , Ha.min "0.1"
                , Ha.step "0.001"
                ]
                []
            , span [] [ model.circleSize |> String.fromFloat |> text ]
            ]
        , dt []
            [ text "画像サイズ" ]
        , dd
            []
            [ Ss.view model.size |> Html.map ChangeSize
            ]
        ]


preview model =
    section [ class "preview space-y-4" ]
        [ h2 [ class "section_title" ] [ text "プレビュー" ]
        , div [ class "svg_container" ] [ svg_area model |> toHtml ]
        , div [ class "button_container" ] [ button [ onClick DownloadSvg ] [ text "ダウンロード" ] ]
        ]


circle_color_input_list model =
    div []
        (List.indexedMap
            (\i picker ->
                Cp.view picker |> Html.map (ChangeCircleColor i)
            )
            model.circleColors
        )


svg_area_text model =
    model |> svg_area |> toString 2


svg_area model =
    svg
        [ Sa.width <| (model.size.width |> String.fromInt)
        , Sa.height <| (model.size.height |> String.fromInt)
        , Sa.viewBox <| (model |> toViewBox)
        ]
        [ circles model
        ]


toViewBox model =
    String.join " " [ "0", "0", model.size.width |> String.fromInt, model.size.height |> String.fromInt ]


circles model =
    let
        colorList =
            model.circleColors |> List.map .color

        ( c, seed ) =
            model.seed
                |> Random.initialSeed
                |> circleList model.num colorList model.size.width model.size.height model.circleSize

        bg =
            rect
                [ Sa.x "0"
                , Sa.y "0"
                , Sa.width <| (model.size.width |> String.fromInt)
                , Sa.height <| (model.size.height |> String.fromInt)
                , fill (model.backgroundColorPicker.color |> colorToHexWithAlpha)
                ]
                []
    in
    g []
        (List.append [ bg ] c)


circleList : Int -> List Color -> Int -> Int -> Float -> Random.Seed -> ( List (Svg Msg), Random.Seed )
circleList length colorList width height circleSize seed =
    let
        ( childViewModels, seed1 ) =
            Random.step
                (genCircleListViewModel length (width |> toFloat) (height |> toFloat) circleSize colorList)
                seed
    in
    ( childViewModels |> List.map circleView, seed1 )


type alias CircleViewModel =
    { x : Float
    , y : Float
    , r : Float
    , color : ( Maybe Color, List Color )
    }


genCircleViewModel : Float -> Float -> Float -> List Color -> Random.Generator CircleViewModel
genCircleViewModel width height size colorList =
    let
        base =
            Basics.min width height

        r =
            base * size
    in
    Random.map4
        CircleViewModel
        (Random.float 0 width)
        (Random.float 0 height)
        (Random.float 0 r)
        (Random.List.choose colorList)


genCircleListViewModel : Int -> Float -> Float -> Float -> List Color -> Random.Generator (List CircleViewModel)
genCircleListViewModel length width height size colorList =
    Random.list length (genCircleViewModel width height size colorList)


circleView : CircleViewModel -> Svg msg
circleView vm =
    circle
        [ vm.x |> String.fromFloat |> cx
        , vm.y |> String.fromFloat |> cy
        , vm.r |> String.fromFloat |> r
        , vm.color
            |> Tuple.first
            |> Maybe.withDefault Color.white
            |> colorToHexWithAlpha
            |> fill
        ]
        []


download : String -> Cmd msg
download svgContent =
    Download.string "image.svg" "image/svg+xml" svgContent
